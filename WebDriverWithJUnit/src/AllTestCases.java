import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class AllTestCases {
	public static WebDriver driver;
	public static  WebDriverWait wait;
	@Before
	public void before()
	{
		System.setProperty("webdriver.chrome.driver","C:/Users/sures/OneDrive/Desktop/Automation/chromedriver.exe");
        driver=new ChromeDriver();
		driver.get("https://www.lkbennett.com/");
	    driver.manage().window().maximize();
	    Assert.assertEquals("L.K.Bennett | Luxury Women�s Clothing & Fashion", driver.getTitle());
	    Assert.assertEquals("https://www.lkbennett.com/", driver.getCurrentUrl());
	}
	
	/*@Test
	public void searchWithValidData()
	{

		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).clear();
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).sendKeys("boots");
		 driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
		
		Assert.assertEquals("BOOTS", driver.findElement(By.cssSelector("h1")).getText());
    }
	@Test
	public void searchWithInvalidData()
	{
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).clear();
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).sendKeys("hgahagdh");
		 driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
		 
		 Assert.assertEquals("https://www.lkbennett.com/search/?text=hgahagdh", driver.getCurrentUrl());
	}
	@Test
	public void searchWithEmptySpace()
	{
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).clear();
	     driver.findElements(By.cssSelector(".input-group .ui-autocomplete-input")).get(0).sendKeys(" ");
		 driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
		 Assert.assertEquals("Search | LKBennett-UK", driver.getTitle()); 
	     Assert.assertEquals("https://www.lkbennett.com/search/?text=+", driver.getCurrentUrl());
         	}
	
	@Test
	public void storeLcatorValidPostcode()
	{
		    Actions action=new Actions(driver);
			WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
	        action.moveToElement(element).perform(); 
	        wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover .icon--primary"))).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
			Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#storelocator-query"))).click();
			//driver.findElement(By.cssSelector("#storelocator-query")).click();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("da1 2xa");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	      Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}
	@Test
	public void storeLocatorInvalidPostcode()
	{
		    Actions action=new Actions(driver);
			WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
	        action.moveToElement(element).perform(); 
	        wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover .icon--primary"))).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
			Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.cssSelector("#storelocator-query")).click();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("abcd");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	     // Assert.assertEquals("stores found", driver.findElement(By.cssSelector("col-lg-9 .store-finder-pagination")).getText());
	}
	@Test
	public void loginWithValidDetails()
	{
		
		WebDriverWait wait=new WebDriverWait(driver,10);
		Actions action=new Actions(driver);
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsSignInbtn.ts-2x")).click();
		Assert.assertEquals("https://www.lkbennett.com/login",driver.getCurrentUrl());
		Assert.assertEquals("Login | L.K.Bennett", driver.getTitle());
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#j_username")));
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("vanam05work@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Padmavathi100");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();
       Assert.assertEquals("https://www.lkbennett.com/", driver.getCurrentUrl());
       Assert.assertEquals("L.K.Bennett | Luxury Women�s Clothing & Fashion", driver.getTitle());
	}
	@Test
	public void loginWithInavalidUsername() {
		WebDriverWait wait=new WebDriverWait(driver,10);
		Actions action=new Actions(driver);
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsSignInbtn.ts-2x")).click();
		Assert.assertEquals("Login | L.K.Bennett", driver.getTitle());
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#j_username")));
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("vanam05work@");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Padmavathi100");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();
		
		
	}
	
	@Test
	public void loginWithInavalidpassword() {
		WebDriverWait wait=new WebDriverWait(driver,10);
		Actions action=new Actions(driver);
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsSignInbtn.ts-2x")).click();
		Assert.assertEquals("Login | L.K.Bennett", driver.getTitle());
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#j_username")));
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("vanam05work@gmail");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("$%^&*");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();
		Assert.assertEquals("https://www.lkbennett.com/login?error=true", driver.getCurrentUrl());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals("Your username or password was incorrect.", driver.findElement(By.cssSelector(".alert.alert-danger")).getText());
		
	}
	@Test
	public void loginWithEmptyUsername()
	{
		WebDriverWait wait=new WebDriverWait(driver,10);
		Actions action=new Actions(driver);
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsSignInbtn.ts-2x")).click();
		Assert.assertEquals("Login | L.K.Bennett", driver.getTitle());
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#j_username")));
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys(" ");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Padmavathi100");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();
		Assert.assertEquals("https://www.lkbennett.com/login?error=true", driver.getCurrentUrl());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals("Your username or password was incorrect.", driver.findElement(By.cssSelector(".alert.alert-danger")).getText());
		
	}
	@Test
	public void LoginWithEmptyUsername()
	{
		WebDriverWait wait=new WebDriverWait(driver,10);
		Actions action=new Actions(driver);
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsSignInbtn.ts-2x")).click();
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#j_username")));
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys(" ");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Padmavathi100");
		driver.findElements(By.cssSelector("#loginForm .btn-block.btn--large")).get(0).click();
		Assert.assertEquals("Your username or password was incorrect.", driver.findElement(By.cssSelector("div.alert.alert-danger.alert-dismissable")).getText());
		
	}
@Test
	public void RegisterWithValidDetails()
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions action=new Actions(driver);
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#jsSignInPopup")));
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsCreateAccountbtn.ts-2x")).click();
		Assert.assertEquals("https://www.lkbennett.com/login",driver.getCurrentUrl());	
		Assert.assertEquals("Login | L.K.Bennett", driver.getTitle());
	    Select title=new Select(driver.findElement(By.id("register.title")));
        title.selectByVisibleText("Mr");
		title.selectByIndex(1);		
		driver.findElement(By.id("register.firstName")).clear();
		driver.findElement(By.id("register.firstName")).sendKeys("mala");
		driver.findElement(By.id("register.lastname")).clear();		
		driver.findElement(By.id("register.lastname")).sendKeys("acha");		
		driver.findElement(By.id("register.email")).clear();		
		driver.findElement(By.id("register.email")).sendKeys("vanam05work@gmail.com");		
		driver.findElement(By.id("register.confirmemail")).clear();
		driver.findElement(By.id("register.confirmemail")).sendKeys("vanam05work@gmail.com");		
		driver.findElement(By.id("register.password")).sendKeys("Padmavathi100");		
		driver.findElement(By.id("register.checkPwd")).sendKeys("Padmavathi100");
		driver.findElement(By.cssSelector("#createAccountButton")).click();
	    Assert.assertEquals("https://www.lkbennett.com/my-account/profile", driver.getCurrentUrl());
		Assert.assertEquals("Thank you for registering.", driver.findElement(By.cssSelector(".alert.alert-info")).getText());
		Assert.assertEquals("Profile | L.K.Bennett", driver.getTitle());
	  
	}
	@Test
	public void registerWithInvalidMailid() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions action=new Actions(driver);
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#jsSignInPopup")));
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsCreateAccountbtn.ts-2x")).click();
		Assert.assertEquals("https://www.lkbennett.com/login",driver.getCurrentUrl());	
		Assert.assertEquals("Login | L.K.Bennett", driver.getTitle());
	    Select title=new Select(driver.findElement(By.id("register.title")));
        title.selectByVisibleText("Mr");
		title.selectByIndex(1);		
		driver.findElement(By.id("register.firstName")).clear();
		driver.findElement(By.id("register.firstName")).sendKeys("mala");
		driver.findElement(By.id("register.lastname")).clear();		
		driver.findElement(By.id("register.lastname")).sendKeys("acha");		
		driver.findElement(By.id("register.email")).clear();		
		driver.findElement(By.id("register.email")).sendKeys("vanam05work");	
		driver.findElement(By.id("register.confirmemail")).clear();
		driver.findElement(By.id("register.confirmemail")).sendKeys("vanam05work@gmail.com");
		driver.findElement(By.id("register.password")).sendKeys("Padmavathi100");		
		driver.findElement(By.id("register.checkPwd")).sendKeys("Padmavathi100");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#createAccountButton"))).click();
		//driver.findElement(By.cssSelector("#createAccountButton")).click();
	    Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  // Assert.assertEquals("Please enter a valid email", driver.findElement(By.cssSelector(".form-group.has-error")).getText());
	
	}
	@Test
	public void registerWithInvalidConfirmMail()
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions action=new Actions(driver);
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#jsSignInPopup")));
		WebElement element=driver.findElement(By.cssSelector("#jsSignInPopup"));
		action.moveToElement(element).perform();
		driver.findElement(By.cssSelector(".info-container .jsCreateAccountbtn.ts-2x")).click();
		Assert.assertEquals("https://www.lkbennett.com/login",driver.getCurrentUrl());	
		Assert.assertEquals("Login | L.K.Bennett", driver.getTitle());
	    Select title=new Select(driver.findElement(By.id("register.title")));
        title.selectByVisibleText("Mr");
		title.selectByIndex(1);		
		driver.findElement(By.id("register.firstName")).clear();
		driver.findElement(By.id("register.firstName")).sendKeys("mala");
		driver.findElement(By.id("register.lastname")).clear();		
		driver.findElement(By.id("register.lastname")).sendKeys("acha");		
		driver.findElement(By.id("register.email")).clear();		
		driver.findElement(By.id("register.email")).sendKeys("vanam05work@gmail.com");	
		driver.findElement(By.id("register.confirmemail")).clear();
		driver.findElement(By.id("register.confirmemail")).sendKeys("vanam05work@");
		driver.findElement(By.id("register.password")).sendKeys("Padmavathi100");		
		driver.findElement(By.id("register.checkPwd")).sendKeys("Padmavathi100");
		//driver.findElement(By.cssSelector("#createAccountButton")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Assert.assertEquals("Please enter a valid confirm email", driver.findElement(By.cssSelector(".error-msg.js-errorMsg")).getText());
	}
	
	@Test
	public void addToBasketWithSearch() throws InterruptedException
	{
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    driver.findElements(By.cssSelector("input[name='text']")).get(0).sendKeys("boots");
	    driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
        Assert.assertEquals("Boots", driver.findElement(By.cssSelector(".breadcrumb .active")).getText());
	    driver.findElement(By.cssSelector("#filterByTop")).click();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.findElements(By.cssSelector(".facet-mark")).get(1).click();  
	    driver.findElement(By.cssSelector("#tab2Title")).click();
	    driver.findElements(By.cssSelector(".facet-mark")).get(2).click();
	    driver.findElements(By.cssSelector(".jsToggleIcons")).get(2).click();
	    driver.findElements(By.cssSelector(".facet-mark")).get(11).click();
	    driver.findElements(By.cssSelector(".jsToggleIcons")).get(3).click();
	    driver.findElements(By.cssSelector("span[data-href='esp_filter_prices=']")).get(2).click();
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.findElement(By.cssSelector("#filterByTop")).click();	  
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    WebElement Element=driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(1);
	    js.executeScript("arguments[0].scrollIntoView();", Element);
		driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(1).click();
		Assert.assertEquals("Kiran Navy Stretch Suede Knee Boots",driver.findElement(By.cssSelector(".breadcrumb .active")).getText());
		driver.findElements(By.cssSelector(".swatch_colour_a")).get(0).click();
		Select title=new Select(driver.findElement(By.cssSelector("#Size")));
		title.selectByIndex(3);
	    driver.findElement(By.cssSelector("#addToCartButton")).click(); 
	    Assert.assertEquals("https://www.lkbennett.com/product/SKKIRANSTRETCHSUEDEBlueNavy~Kiran-Navy-Stretch-Suede-Knee-Boots-Navy", driver.getCurrentUrl());
	    

	}
	
	@Test
	public void addProductByHover() throws InterruptedException
	{
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    Actions action=new Actions(driver);
			WebElement element=driver.findElements(By.cssSelector("a[title='Shoes']")).get(1);
	        action.moveToElement(element).perform();
	        driver.findElement(By.cssSelector("a[title='Boots']")).click();
	       Assert.assertEquals("Boots", driver.findElement(By.cssSelector(".breadcrumb .active")).getText());
	        JavascriptExecutor js = (JavascriptExecutor) driver;
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.findElement(By.cssSelector("#filterByTop")).click();
		    driver.findElements(By.cssSelector(".facet-mark")).get(1).click();
		    driver.findElement(By.cssSelector("#tab2Title")).click();
		    driver.findElements(By.cssSelector(".facet-mark")).get(2).click();
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.findElements(By.cssSelector(".jsToggleIcons")).get(2).click();
		    driver.findElements(By.cssSelector(".facet-mark")).get(11).click();
		    driver.findElements(By.cssSelector(".jsToggleIcons")).get(3).click();
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.findElements(By.cssSelector("span[data-href='esp_filter_prices=']")).get(2).click();
		    driver.findElement(By.cssSelector("#filterByTop")).click();
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    WebElement Element=driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(1);
		    js.executeScript("arguments[0].scrollIntoView();", Element);
		    driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(1).click();
		   driver.findElements(By.cssSelector(".swatch_colour_a")).get(0).click();
		    Select title=new Select(driver.findElement(By.cssSelector("#Size")));
		    title.selectByIndex(3);
		    driver.findElement(By.cssSelector("#addToCartButton")).click(); 
           Assert.assertEquals("https://www.lkbennett.com/product/SKKIRANSTRETCHSUEDEBlueNavy~Kiran-Navy-Stretch-Suede-Knee-Boots-Navy", driver.getCurrentUrl());
	}

	@Test
	public void adproductToBasketOutofstockItem()
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    driver.findElements(By.cssSelector("input[name='text']")).get(0).sendKeys("boots");
	    driver.findElements(By.cssSelector(".input-group-btn .btn-link .icon--primary")).get(0).click();
        Assert.assertEquals("Boots", driver.findElement(By.cssSelector(".breadcrumb .active")).getText());
	    driver.findElement(By.cssSelector("#filterByTop")).click();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.findElements(By.cssSelector(".facet-mark")).get(1).click();  
	    driver.findElement(By.cssSelector("#tab2Title")).click();
	    driver.findElements(By.cssSelector(".facet-mark")).get(2).click();
	    driver.findElements(By.cssSelector(".jsToggleIcons")).get(2).click();
	    driver.findElements(By.cssSelector(".facet-mark")).get(11).click();
	    driver.findElements(By.cssSelector(".jsToggleIcons")).get(3).click();
	    driver.findElements(By.cssSelector("span[data-href='esp_filter_prices=']")).get(2).click();
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.findElement(By.cssSelector("#filterByTop")).click();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    WebElement Element=driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(1);
	    js.executeScript("arguments[0].scrollIntoView();", Element);
		driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(1).click();
		Assert.assertEquals("Kiran Navy Stretch Suede Knee Boots",driver.findElement(By.cssSelector(".breadcrumb .active")).getText());
		driver.findElements(By.cssSelector(".swatch_colour_a")).get(0).click();
		Select title=new Select(driver.findElement(By.cssSelector("#Size")));
		title.selectByIndex(3);
	    driver.findElement(By.cssSelector("#addToCartButton")).click(); 	
	   // Assert.assertEquals("This product is out of stock", driver.findElement(By.cssSelector(".customSelectParent .js-productAvailability")).getText());
	}*/
	@Test
	public void checkoutWithHomeDelivery() throws InterruptedException
	{
    WebDriverWait wait=new WebDriverWait(driver,10);
	wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("a[title='Shoes']")));
    Actions action=new Actions(driver);
	WebElement element=driver.findElements(By.cssSelector("a[title='Shoes']")).get(2);
    action.moveToElement(element).perform();
    driver.findElement(By.cssSelector("a[title='Boots']")).click();
    JavascriptExecutor js = (JavascriptExecutor) driver;
  
    driver.findElement(By.cssSelector("#filterByTop")).click();
    
    driver.findElements(By.cssSelector(".facet-mark")).get(1).click();
    driver.findElement(By.cssSelector("#tab2Title")).click();
    driver.findElements(By.cssSelector(".facet-mark")).get(2).click();
   
    driver.findElements(By.cssSelector(".jsToggleIcons")).get(2).click();
    driver.findElements(By.cssSelector(".facet-mark")).get(11).click();
    driver.findElements(By.cssSelector(".jsToggleIcons")).get(3).click();
    driver.findElements(By.cssSelector("span[data-href='esp_filter_prices=']")).get(2).click();
    driver.findElement(By.cssSelector("input[type='submit']")).click();
    Thread.sleep(3000);
    WebElement Element=driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(3);
    js.executeScript("arguments[0].scrollIntoView();", Element);
    driver.findElements(By.cssSelector("#jsPlp .thumb img")).get(3).click();
    Thread.sleep(3000);
    Thread.sleep(3000);
    Select title=new Select(driver.findElement(By.cssSelector("#Size")));
    title.selectByIndex(3);
    driver.findElement(By.cssSelector("#addToCartButton")).click(); 
    Thread.sleep(9000);
    Actions actions=new Actions(driver);
	WebElement element1=driver.findElements(By.cssSelector("#popoverData .icon--primary.header-lastlink--parent .header-lastlink--content .jsMiniCartCount")).get(1);
    actions.moveToElement(element1).perform();
    Thread.sleep(3000);
    driver.findElement(By.cssSelector("a[style='display: block;']")).click();
    Thread.sleep(3000);
    driver.findElement(By.cssSelector(".cart-top-section .hidden-sm.text-right .btn--mdm.checkoutButton")).click();
    Thread.sleep(3000);
    driver.findElement(By.cssSelector("#j_username")).clear();
    driver.findElement(By.cssSelector("#j_username")).sendKeys("malagopathi@gmail.com");
    driver.findElement(By.cssSelector("#j_password")).clear();
    driver.findElement(By.cssSelector("#j_password")).sendKeys("Padmavathi100");
    driver.findElement(By.cssSelector("#loginForm .col-center.text-center .btn-block")).click();
    driver.findElement(By.cssSelector(".cart-top-section .hidden-sm.text-right .btn--mdm.checkoutButton")).click();
    driver.findElement(By.linkText("Deliver my Order")).click();
   
	}
	/*
	@Test
	public void addProductBasketWithoutsize()
	{
		
	}
	@Test
	public void addToBasketMaxQuantity()
	{
		
	}
	@Test
	public void storeLocatorWithVaidPostcode()
	{       
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    Actions action=new Actions(driver);
			WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
	        action.moveToElement(element).perform(); 
	        driver.findElement(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover")).click();
	        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	        driver.findElement(By.cssSelector("#storelocator-query")).clear();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("da1 2xa");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}
	@Test
	public void storeLocatorWithinVaidPostcode()
	{
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    Actions action=new Actions(driver);
			WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
	        action.moveToElement(element).perform(); 
	        driver.findElement(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover")).click();
	        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	        driver.findElement(By.cssSelector("#storelocator-query")).clear();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("da1");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	    Assert.assertEquals("https://www.lkbennett.com/store-finder", driver.getCurrentUrl());
	}
	@Test
	public void storeLocatorWithemptyPostcode()
	{
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    Actions action=new Actions(driver);
			WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
	        action.moveToElement(element).perform(); 
	        driver.findElement(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover")).click();
	        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	        driver.findElement(By.cssSelector("#storelocator-query")).clear();
	        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("  ");
	        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
	        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}
	@Test 
	public void storeLocatorWithspecial_chars()
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    Actions action=new Actions(driver);
		WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
        action.moveToElement(element).perform(); 
        driver.findElement(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover")).click();
        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
        driver.findElement(By.cssSelector("#storelocator-query")).clear();
        driver.findElement(By.cssSelector("#storelocator-query")).sendKeys("$%^&*");
        driver.findElements(By.cssSelector(".icon-Search.icon-2x")).get(4).click();
        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}
	@Test
	public void findStoreNear()
	{
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    Actions action=new Actions(driver);
		WebElement element=driver.findElements(By.cssSelector(".icon-store-locator.icon-2x.icon--primary")).get(0);
        action.moveToElement(element).perform(); 
        driver.findElement(By.cssSelector(".anchor--plain.jsstorepopover.storeFinderPopover")).click();
        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());    
        driver.findElement(By.cssSelector("#findStoresNearMe")).click();
        Assert.assertEquals("Store Locator | L.K.Bennett", driver.getTitle());
	}
	@Test
	public void toaccesssvedItemsLogin()
	{
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    Actions action=new Actions(driver);
			WebElement element=driver.findElements(By.cssSelector(".icon-wishlist.icon-2x")).get(0);
	        action.moveToElement(element).perform();
	        driver.findElements(By.cssSelector(".info-container .btn.btn--transparent")).get(0).click();
	        Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
	}
	@Test
	public void toaccesssavedItemsRegister()
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions action=new Actions(driver);
		WebElement element=driver.findElements(By.cssSelector(".icon-wishlist.icon-2x")).get(0);
        action.moveToElement(element).perform();
        driver.findElements(By.cssSelector(".info-container .btn")).get(1).click();
        Assert.assertEquals("https://www.lkbennett.com/login", driver.getCurrentUrl());
	}
	@Test
	public void GBP()
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Actions action=new Actions(driver);
		WebElement element=driver.findElement(By.cssSelector(".image-currency-globe"));
        action.moveToElement(element).perform();
        WebElement Element=driver.findElements(By.cssSelector("a[data-iso='GB']")).get(0);
	    js.executeScript("arguments[0].scrollIntoView();", Element);
	    driver.findElements(By.cssSelector("a[data-iso='GB']")).get(0).click();
	    Assert.assertEquals("https://www.lkbennett.com/", driver.getCurrentUrl());
	}
	@Test
	public void footerfb()
	{
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		    driver.findElement(By.cssSelector(".continue-browse__link")).click();
	        driver.findElement(By.cssSelector(".icon-Facebook")).click();
	        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	       // Assert.assertEquals("https://www.facebook.com/LKBennettLondon", driver.getCurrentUrl());
	}
	@Test
	public void footerinsta()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();
	    driver.findElement(By.cssSelector(".icon-Instagram.icon--primary.icon-2x")).click();
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    
   //Assert.assertEquals("https://www.instagram.com/lkbennettlondon/", driver.getCurrentUrl());
	}
   @Test
	public void footertwitter()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();
	    driver.findElement(By.cssSelector(".icon-Twitter.icon--primary.icon-2x")).click();
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    Assert.assertEquals("https://twitter.com/LKBennettLondon", driver.getCurrentUrl());
	}
	@Test
	public void footeryoutube()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();	   
	    driver.findElement(By.cssSelector(".icon-Youtube.icon--primary.icon-2x")).click();
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
Assert.assertEquals("https://www.youtube.com/user/lkbennettlondon", driver.getCurrentUrl());	
	}
	@Test
	public void footerpinterest()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	    driver.findElement(By.cssSelector(".continue-browse__link")).click();	  
	    driver.findElement(By.cssSelector(".icon-Pinterest.icon--primary.icon-2x")).click();
	    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	    Assert.assertEquals("https://www.pinterest.co.uk/lkbennettlondon/", driver.getCurrentUrl());
	}*/
	@After
    public void after()
    {
		System.out.println("I am in after");
		//driver.close();
    }
	
}
